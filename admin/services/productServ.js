
const BASE_URL = "https://648eedd875a96b6644447c06.mockapi.io/Products";
var productServ = {
    getList: () =>{
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
delete: (id) => {
    return axios({
        url: `${BASE_URL}/${id}`,
        method:"DELETE",
    });
    },
create:(Products) =>{ //tạo
        return axios({
            url: BASE_URL, // bắt buộc có
            method: "POST", //bắt buộc có
            data: Products, //tạo mới, update mới có
        }); 
    },

getById: function(id) {
        return axios({
            url:`${BASE_URL}/${id}`,
            method: "GET",
        });//sửa
    },
update: function(id,product){
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data:product,
        });
    },
};
// nơi định nghĩa những API