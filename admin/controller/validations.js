
function showMessage(idSpan, message){
    document.getElementById(idSpan).innerText = message;
}

function kiemTraNull(item_array_product, idSpan, message){
    if(item_array_product != "") {
        showMessage(idSpan, "");
        return true;
    }else {
        showMessage(idSpan, message);
        return false;
    }
}

function kiemTraType(item_array_product, idSpan, message){
    if(item_array_product == "Iphone" || item_array_product == "Samsung") {
        showMessage(idSpan, "");
        return true;
    }else {
        showMessage(idSpan, message);
        return false;
    }
}