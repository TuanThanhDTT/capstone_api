function renderProductList(productArr){
    //productArr là mảng
    //render API ra bang
    var contentHTML=""; //bien chua cac mang
    
    productArr.reverse().forEach (function(item){ //item là tham số, item là phần tử trong productARR. foreach lấy từng phần tử đó đưa cho mình
        //reverse là arr đảo ngược
        var content = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td><img src="${item.img}" alt="">
        </td>
        <td>${item.desc}</td>
        <td>
        <button onclick="xoaSP(${item.id})" class="btn btn-danger mb-3">Xoá</button>
        <button onclick="suaSP(${item.id})" class="btn btn-warning">Sửa</button>
        </td>
        </tr>`;
        contentHTML +=content;
    });
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;


    
}
function layThongTinTuForm(){
    var tenSP = document.getElementById("TenSP").value;
    var giaSP = document.getElementById("GiaSP").value*1;
    var hinhSP = document.getElementById("HinhSP").value;
    var motaSP = document.getElementById("MotaSP").value;
    var screen = document.getElementById("screenSP").value;
    var backCamera = document.getElementById("backCameraSP").value;
    var frontCamera = document.getElementById("frontCameraSP").value;
    var type = document.getElementById("typeSP").value;
    
    // k có chức năng id trong create
    return{
        name: tenSP,
        price: giaSP,
        img: hinhSP,
        desc: motaSP,
        screen: screen,
        backCamera: backCamera,
        frontCamera: frontCamera,
        type: type,
        desc: motaSP,
        //trả theo trong bảng mocAPI
    };
}
function showThongTinLenForm(Products){
   document.getElementById("TenSP").value = Products.name;
   document.getElementById("GiaSP").value= Products.price;
   document.getElementById("HinhSP").value= Products.img;
   document.getElementById("MotaSP").value= Products.desc;
   document.getElementById("screenSP").value= Products.screen;
   document.getElementById("backCameraSP").value= Products.backCamera;
   document.getElementById("frontCameraSP").value= Products.frontCamera;
   document.getElementById("typeSP").value= Products.type;

}

let showSuccess = (title = "Thành Công") => {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title,
        showConfirmButton: false,
        timer: 1500
      });
};

let openModalButton = document.getElementById("btnThemSP");
openModalButton.addEventListener("click", function () {
  document.getElementById("btn_capnhatSP").disabled = true;
  document.getElementById("btn_themSP").disabled = false;
});
