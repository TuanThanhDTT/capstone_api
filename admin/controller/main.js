let idProductUpdate = null;


function fetchProductList(){
    productServ.getList()
    .then(function(res){
        renderProductList(res.data) // then là gọi 
    })
    .catch(function(err){
    });
}

fetchProductList(); //gọi hàm

//xoaSP
function xoaSP(id){ //xóa sp trên sever
    productServ.delete(id)
    .then(function(res){
        fetchProductList();//sau khi xóa thành công trên sv thì gọi api danh sách mới nhất từ sv
        showSuccess("Đã xóa sản phẩm!");
    })
    .catch(function(err){
    });
}
function themSP(){
    let newProduct = layThongTinTuForm();
    let elements = document.getElementsByClassName("spanMessage");
    for (var i = 0; i < elements.length; i++) {
      elements[i].style.display = "block";
    }
    //kiểm tra tên sản phẩm
    let isValid = kiemTraNull(newProduct.name,"form-message-TenSP","Tên sản phẩm không được để trống!");
    
    //kiểm tra giá
    isValid &= kiemTraNull(newProduct.price,"form-message-GiaSP","Chưa nhập giá sản phẩm!");

    //kiểm tra các thông tin khác
    isValid &= kiemTraNull(newProduct.img,"form-message-HinhSP","Chưa nhập link hình ảnh sản phẩm!");
    isValid &= kiemTraNull(newProduct.screen,"form-message-screenSP","Chưa nhập thông tin màn hình sản phẩm!");
    isValid &= kiemTraNull(newProduct.backCamera,"form-message-backCameraSP","Chưa nhập thông tin backCamera sản phẩm!");
    isValid &= kiemTraNull(newProduct.frontCamera,"form-message-frontCameraSP","Chưa nhập thông tin frontCamera sản phẩm!");
    isValid &= kiemTraNull(newProduct.type,"form-message-typeSP","Chưa nhập thông tin type sản phẩm là Iphone hay Samsung!") && kiemTraType(newProduct.type,"form-message-typeSP","Type sản phẩm phải là Iphone hoặc Samsung , vui lòng chú ý xóa khoảng trắng ở cuối");
    isValid &= kiemTraNull(newProduct.desc,"form-message-MotaSP","Chưa nhập thông tin mô tả sản phẩm!");

    if(isValid) {
        productServ
        .create(newProduct)
        .then(function(res){ //then catch k thể tái sử dụng được, khi thành công
            showSuccess("Đã thêm sản phẩm!");
            $("#myModal").modal("hide");
            fetchProductList();
            // nếu thêm thành công thì gọi api lấy dsach mới nhất từ sv
        })
        .catch(function(err){});//khi thất bại
    }

}
function suaSP(id){
    idProductUpdate = id;
    document.getElementById("btn_capnhatSP").disabled = false;
    document.getElementById("btn_themSP").disabled = true;
    $("#myModal").modal("show");
    //dua thong tin len form
    productServ
    .getById(id)
    .then (function (res){
        showThongTinLenForm(res.data);
    })
    .catch(function(err){});
}
function capNhatSP(){
    let product = layThongTinTuForm();
    let elements = document.getElementsByClassName("spanMessage");
    for (var i = 0; i < elements.length; i++) {
      elements[i].style.display = "block";
    }
    //kiểm tra tên sản phẩm
    let isValid = kiemTraNull(product.name,"form-message-TenSP","Tên sản phẩm không được để trống!");
    
    //kiểm tra giá
    isValid &= kiemTraNull(product.price,"form-message-GiaSP","Chưa nhập giá sản phẩm!");

    //kiểm tra các thông tin khác
    isValid &= kiemTraNull(product.img,"form-message-HinhSP","Chưa nhập link hình ảnh sản phẩm!");
    isValid &= kiemTraNull(product.screen,"form-message-screenSP","Chưa nhập thông tin màn hình sản phẩm!");
    isValid &= kiemTraNull(product.backCamera,"form-message-backCameraSP","Chưa nhập thông tin backCamera sản phẩm!");
    isValid &= kiemTraNull(product.frontCamera,"form-message-frontCameraSP","Chưa nhập thông tin frontCamera sản phẩm!");
    isValid &= kiemTraNull(product.type,"form-message-typeSP","Chưa nhập thông tin type sản phẩm là Iphone hay Samsung") && kiemTraType(product.type,"form-message-typeSP","Type sản phẩm phải là Iphone hoặc Samsung, vui lòng chú ý xóa khoảng trắng ở cuối");
    isValid &= kiemTraNull(product.desc,"form-message-MotaSP","Chưa nhập thông tin mô tả sản phẩm!");
    if(isValid){
        productServ
        .update(idProductUpdate, product)
        .then(function(res){
            showSuccess("Đã cập nhật sản phẩm!");
            $("#myModal").modal("hide");
            fetchProductList();
        })
        .catch(function(err){});
    }

}

//Chức năng tìm kiếm sản phẩm theo tên
function filterProduct(){
    productServ.getList().then((res) => {
            let search = document.getElementById("search").value;
            if(search != ""){
              dssp = res.data;
              let filterProduct = dssp.filter(function(item_dssp){
                    return item_dssp.name == search;
              });
              renderProductList(filterProduct);
            } else {
              renderProductList(dssp);
            }
          })
          .catch((err) => {
           console.log(err);
          });
  }
  document.getElementById("selectProduct").addEventListener("click", function () {
    filterProduct();
  });

// Chức năng sắp xếp sản phẩm dựa vào tăng giảm theo giá
function sapXepTheoGia(products) {
    products.sort((a, b) => a.price - b.price);
  }

document.getElementById("sortdown").addEventListener("click",function(){
    productServ.getList().then((res) => { 
        let dssp = res.data;
        sapXepTheoGia(dssp);
        console.log("sort1",dssp);
        renderProductList(dssp);
    })
    .catch((err) => {});
});

document.getElementById("sortup").addEventListener("click",function(){
    productServ.getList().then((res) => {
        let dssp = res.data;
        sapXepTheoGia(dssp);
        dssp.reverse();
        console.log("sort2",dssp);
        renderProductList(dssp);
    })
    .catch((err) => {});
});





