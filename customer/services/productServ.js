const BASE_URL = "https://648eedd875a96b6644447c06.mockapi.io/Products";
var productServ = {
    getList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    delete: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        });
    },
    create: (product) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: product,
        });
    },
    getByID: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        });
    },
    update: (id, product) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: product,
        });
    },
};