
function fetchProductList(){
  productServ.getList()
  .then(function(res){
      dssp = res.data;
      renderProductList(dssp);
  }).catch(function(err){
      console.log("Error", err);
  })
}
fetchProductList();

//Chức năng lọc danh sách sản phẩm từ API
//Cách 1 dùng onchange gọi hàm cho select ở html
//Cách 2 dùng addEventListener gắn sự kiện onclick và chạy function
function filterProduct(){
  productServ.getList().then((res) => {
          let selectProduct = document.getElementById("selectProduct").value;
          if(selectProduct != "All"){
            console.log(selectProduct);
            dssp = res.data;
            let filterProduct = dssp.filter(function(item_dssp){
                  return item_dssp.type == selectProduct;
            });
            renderProductList(filterProduct);
          } else {
            renderProductList(dssp);
          }
        })
        .catch((err) => {
         console.log(err);
        });
}

//Lấy thông tin sản phẩm user chọn, fill vào layout giỏ hàng
let cart = [];
let getDataJson = localStorage.getItem("DSSP");
if(getDataJson != null){
  cart = JSON.parse(getDataJson);
  console.log("cart",cart);
  renderCart(cart);
  let countItemCartHTML = cart.reduce((total, item) => {
    return total + item.quantity;
  }, 0);
    document.getElementById("item-count").style.display = "inline-block";
    document.getElementById("item-count").innerHTML = countItemCartHTML;
    if (countItemCartHTML == 0) {
      document.getElementById("item-count").style.display = "none";
    }
  };
  //Sau khi lấy được thông tin mới thêm vào giỏ hàng
function addToCart(id){
  productServ.getByID(id)
  .then(function(res){
      dssp = res.data;
      let cartItem = {
        id: dssp.id,
        name: dssp.name,
        price: dssp.price,
        img: dssp.img,
        screen: dssp.screen,
        backCamera: dssp.backCamera,
        frontCamera: dssp.frontCamera,
        quantity: 1,
      };
      // Kiểm tra xem sản phẩm đã có trong giỏ hàng hay chưa
      let checkItem = cart.find(item => item.id === dssp.id);
      if (checkItem) {
        // Sản phẩm đã có trong giỏ hàng, tăng quantity lên 1
        checkItem.quantity++;
        renderCart(cart);
      } else {
        // Sản phẩm chưa có trong giỏ hàng, thêm sản phẩm mới với quantity là 1
        cart.push(cartItem);
        renderCart(cart);
      }
      let totalQuantity = cart.reduce((total, item) => {
        return total + item.quantity;
      }, 0);
      document.getElementById("item-count").style.display = "inline-block";
      document.getElementById("item-count").innerHTML = totalQuantity;
      //Lưu cart xuống localStorage
      let setDataJson = JSON.stringify(cart);
      localStorage.setItem("DSSP", setDataJson);
      showSuccess();
    })
    .catch(function(err){
      console.log("Error", err);
  });
}

//Giảm quantity khi nhấn button
function giamQuantity(id){
  let itemsToRemove = [];
  for(let i = 0; i < cart.length; i++){
    if(cart[i].id == id){
      cart[i].quantity--;
      renderCart(cart);
      if(cart[i].quantity == 0){
        itemsToRemove.push(i);
      };
    };
  };
  // xóa item ra khỏi cart khi quantity = 0
  for ( i = itemsToRemove.length - 1; i >= 0; i--) {
    cart.splice(itemsToRemove[i], 1);
    renderCart(cart);
  };
  let totalQuantityDown = cart.reduce((total, item) => {
    return total + item.quantity;
  }, 0);
  document.getElementById("item-count").style.display = "inline-block";
  document.getElementById("item-count").innerHTML = totalQuantityDown;
  let setDataJson = JSON.stringify(cart);
      localStorage.setItem("DSSP", setDataJson);
}


//Tăng quantity khi nhấn button
function tangQuantity(id){
  for(let i = 0; i < cart.length; i++){
    if(cart[i].id == id){
      cart[i].quantity++;
      renderCart(cart);
      let totalQuantityUp = cart.reduce((total, item) => {
        return total + item.quantity;
      }, 0);
      document.getElementById("item-count").style.display = "inline-block";
      document.getElementById("item-count").innerHTML = totalQuantityUp;
      let setDataJson = JSON.stringify(cart);
      localStorage.setItem("DSSP", setDataJson);
    };
  };
}

// Gọi function tại nút PayNow khi người dùng click vào button sẽ set mảng cart về []
document.getElementById("payNow").addEventListener("click", function() {
    cart = [];
    renderCart(cart);
    let countItemCartHTML = cart.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
    if (countItemCartHTML == 0) {
      document.getElementById("item-count").style.display = "none";
    }
    let setDataJson = JSON.stringify(cart);
    localStorage.setItem("DSSP", setDataJson);
    showSuccess("Đã Thanh Toán!");
  } );


  function removeItem(id) {
    let itemsToRemove = [];
  for(let i = 0; i < cart.length; i++){
    if(cart[i].id == id){
        itemsToRemove.push(i);
    };
  };
  for ( i = itemsToRemove.length - 1; i >= 0; i--) {
    cart.splice(itemsToRemove[i], 1);
    renderCart(cart);
  };
  let totalQuantityHTML = cart.reduce((total, item) => {
    return total + item.quantity;
  }, 0);
  document.getElementById("item-count").style.display = "inline-block";
  document.getElementById("item-count").innerHTML = totalQuantityHTML;
  let setDataJson = JSON.stringify(cart);
    localStorage.setItem("DSSP", setDataJson);
    showSuccess("Đã xóa sản phẩm!");
  }
  













