function renderProductList(productArr){
    let contentHTML = "";
    productArr.forEach(function(item){
        let content = 
        `<div class="item">
        <div class="blogs__overlay">
            <div class="blogs__text flex flex-col justify-center space-y-1">
                <h2 class="text-5xl text-yellow-100">Specifications</h2>
                <p id="pd1">Screen: ${item.screen}</p>
                <p id="pd2">BackCamera: ${item.backCamera}</p>
                <p id="pd3">FrontCamera: ${item.frontCamera}</p>
                <div class="text_Desc">
                <p id="des_item">Description: ${item.desc}</p>
                </div>
                <button onclick = "addToCart(${item.id})" class="flex select-none items-center gap-3 rounded-lg bg-pink-500 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-pink-500/20 transition-all hover:shadow-lg hover:shadow-pink-500/40 focus:opacity-[0.85] focus:shadow-none active:opacity-[0.85] active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                type="button"
                data-ripple-light="true" >
                <svg xmlns="http://www.w3.org/2000/svg" fill="none"
                  viewBox="0 0 24 24"
                  stroke-width="2"
                  stroke="currentColor"
                  aria-hidden="true"
                  class="h-5 w-5" >
                  <path stroke-linecap="round"
                    stroke-linejoin="round"
                    d="M17.593 3.322c1.1.128 1.907 1.077 1.907 2.185V21L12 17.25 4.5 21V5.507c0-1.108.806-2.057 1.907-2.185a48.507 48.507 0 0111.186 0z"
                  ></path>
                </svg>
                Add to Cart
              </button>
            </div>
        </div>
        <div class="img_wrapper">
            <img src="${item.img}">
        </div>
        <div class="descr">
            <div class="title">
                <h3 id="name_pd">${item.name}</h3>
                <p id="price_pd">Price: ${item.price}</p>
                <div class="cardPhone__rating mb-5">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="rate">
                <button class="btnPhone-shadow"><i class="fa fa-shopping-cart"></i> Buy Now</button>
            </div>
        </div>
    </div>`
        contentHTML =  contentHTML + content;
    });   
    document.getElementById("list_items").innerHTML = contentHTML;
}

function renderCart(cart){
    let contentHTML_Cart = "";
    let total = 0;
    cart.forEach(function(cartItem) {
        let content_Cart = `
        <br>
        <div class="bodyCartUp flex">
        <div id="img_Cart">
            <img src="${cartItem.img}" alt="">
        </div>
        <div id="desc_Cart">
            <p>Product: ${cartItem.name}</p>
            <p>Screen:  ${cartItem.screen}</p>
            <p>BackCamera: ${cartItem.backCamera}</p>
            <p>FrontCamera: ${cartItem.frontCamera}</p>
        </div>
    </div>

    <div class="bodyCartDown flex">
        <div class="content_left">
            <span>Quantity: 
                <button onclick = "giamQuantity(${cartItem.id})"><i class="fa fa-minus-circle"></i></button>
                <span id ="quantitySP">${cartItem.quantity}</span>
                <button onclick ="tangQuantity(${cartItem.id})"><i class="fa fa-plus-circle"></i></button>
                <br>
                <button id ="removeItem" onclick = "removeItem(${cartItem.id})" >Remove</button>
        </div>
        <div class="content_right">
        <p>Giá: (${cartItem.price}) * Số lượng: (${cartItem.quantity})</p> 
        <p>Thành tiền: ${cartItem.price * cartItem.quantity}$</p>
        </div>
    </div>
    <br>
    <hr>
    `
    contentHTML_Cart = contentHTML_Cart + content_Cart;
    total += cartItem.price * cartItem.quantity;
    });
    contentHTML_Cart += `<br><br><p>Tổng hóa đơn các sản phẩm: ${total}$</p>`;
    document.getElementById("bodyCart").innerHTML = contentHTML_Cart;
}

let showSuccess = (title = "Thành Công") => {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title,
        showConfirmButton: false,
        timer: 1500
      });
};